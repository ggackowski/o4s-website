import React from 'react'
import lg from './lg.jpg'
import style from './Logo.css'

const Logo = () => {

  return (
    <img src={lg} alt="o4s logo"></img>
  );

}

export default Logo;