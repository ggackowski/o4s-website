import React, { Component } from 'react'
import Logo from './components/Logo'
import Text from './components/Text'
import style from './App.css'

class App extends Component  {
  render() {
    return (
    <main>
      <Logo />
      <Text />
    </main>
    );
  }
}

export default App;
